package com.example.calculadora_de_juntadas;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SQLiteConnection extends SQLiteOpenHelper {
    //Table PartyDB
    private static final String CREATE_TABLE_PARTY = "CREATE TABLE PartyDB " +
            "(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT, " +
            "Date TEXT, Description TEXT, Concluded INTEGER)";
    private static final String DESTROY_TABLE_PARTY = "DROP TABLE IF EXISTS PartyDB";

    //Table MemberDB
    private static final String CREATE_TABLE_MEMBER = "CREATE TABLE MemberDB " +
            "(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT, Balance REAL," +
            "Party_id INTEGER, FOREIGN KEY (Party_id) REFERENCES PartyDB (Id))";
    private static final String DESTROY_TABLE_MEMBER = "DROP TABLE IF EXISTS MemberDB";

    //Table SupplyDB
    private static final String CREATE_TABLE_SUPPLY = "CREATE TABLE SupplyDB " +
            "(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT, Cost REAL, " +
            "Description TEXT, Party_id INTEGER, FOREIGN KEY (Party_id) REFERENCES PartyDB (Id))";
    private static final String DESTROY_TABLE_SUPPLY = "DROP TABLE IF EXISTS SupplyDB";

    //Table PurchasesDB
    private static final String CREATE_TABLE_PURCHASE = "CREATE TABLE PurchaseDB " +
            "(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Member_id INTEGER, " +
            "Supply_id INTEGER, Amount REAL," +
            "FOREIGN KEY (Member_id) REFERENCES MemberDB (Id), " +
            "FOREIGN KEY (Supply_id) REFERENCES SupplyDB (Id))";
    private static final String DESTROY_TABLE_PURCHASE = "DROP TABLE IF EXISTS PurchaseDB";

    //Table ConsumerDB
    private static final String CREATE_TABLE_CONSUMER = "CREATE TABLE ConsumerDB " +
            "(Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Member_id INTEGER, " +
            "Supply_id INTEGER," +
            "FOREIGN KEY (Member_id) REFERENCES MemberDB (Id), " +
            "FOREIGN KEY (Supply_id) REFERENCES SupplyDB (Id))";
    private static final String DESTROY_TABLE_CONSUMER = "DROP TABLE IF EXISTS ConsumerDB";

    //Constructor
    public SQLiteConnection(@Nullable Context context, @Nullable String tableName) {
        super(context, tableName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PARTY);
        db.execSQL(CREATE_TABLE_MEMBER);
        db.execSQL(CREATE_TABLE_SUPPLY);
        db.execSQL(CREATE_TABLE_PURCHASE);
        db.execSQL(CREATE_TABLE_CONSUMER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DESTROY_TABLE_PARTY);
        db.execSQL(DESTROY_TABLE_MEMBER);
        db.execSQL(DESTROY_TABLE_SUPPLY);
        db.execSQL(DESTROY_TABLE_PURCHASE);
        db.execSQL(DESTROY_TABLE_CONSUMER);
        onCreate(db);
    }
}

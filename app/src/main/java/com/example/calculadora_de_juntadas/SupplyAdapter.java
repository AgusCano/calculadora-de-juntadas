package com.example.calculadora_de_juntadas;
import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class SupplyAdapter
        extends RecyclerView.Adapter<SupplyAdapter.ViewHolderSupply>
        implements View.OnClickListener {
    private ArrayList<Supply> supplyList;
    private OnItemClickListener supplyListener;


    public SupplyAdapter(ArrayList<Supply> SupplyList) {
        this.supplyList = SupplyList;
    }

    @Override
    public void onClick(View v) {

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.supplyListener = listener;
    }

    public interface OnItemClickListener {
        void onDeleteSupply(String supplyName, int position, Boolean bool);
    }

    public class ViewHolderSupply extends RecyclerView.ViewHolder {
        TextView supplyName, supplyCost;
        ImageView deleteSupply;


        public ViewHolderSupply(View itemView, final OnItemClickListener listener) {
            super(itemView);
            supplyName = itemView.findViewById(R.id.supply_name);
            supplyCost = itemView.findViewById(R.id.supply_cost);

            deleteSupply = itemView.findViewById(R.id.supply_remove_member);

            deleteSupply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        String sName = supplyName.getText().toString();
                        listener.onDeleteSupply(sName, 0, Boolean.TRUE);
                    }
                }
            });
        }
    }

    @Override
    public SupplyAdapter.ViewHolderSupply onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.supply, parent, false);
        view.setOnClickListener(this);

        return new ViewHolderSupply(view, supplyListener);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(SupplyAdapter.ViewHolderSupply holder, int position) {
        holder.supplyName.setText(supplyList.get(position).getSupplyName());

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.supplyCost.setText(df.format(supplyList.get(position).getSupplyCost()));
    }

    @Override
    public int getItemCount() {
        return supplyList.size();
    }
}

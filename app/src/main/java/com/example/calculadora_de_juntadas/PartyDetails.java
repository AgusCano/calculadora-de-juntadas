package com.example.calculadora_de_juntadas;
import android.content.Intent;
import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
        import androidx.appcompat.widget.Toolbar;
        import androidx.viewpager.widget.ViewPager;
        import androidx.appcompat.app.AppCompatActivity;
        import com.example.calculadora_de_juntadas.ui.main.SectionsPagerAdapter;


public class PartyDetails extends AppCompatActivity {
    private final static String QUERY = "SELECT * FROM PartyDB WHERE Id = ";
    public Long pID;
    private SQLiteConnection sqLiteConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.party_details);

        Long pid = getIntent().getLongExtra("PartyID", 1);
        this.pID = pid;
        String partyName = getPartyName(pid);
        String partyDate = getPartyDate(pid);
        toolbarInit(partyName, partyDate);
        viewPagerInit(pid);
    }

    private String getPartyName(Long npid) {
        sqLiteConnection = new SQLiteConnection(this, "primaryDB");
        SQLiteDatabase db = sqLiteConnection.getReadableDatabase();

        Cursor cursor = db.rawQuery(QUERY + npid.toString(), null);
        cursor.moveToFirst();
        String partyName = cursor.getString(1);

        cursor.close();
        db.close();
        return partyName;
    }

    private String getPartyDate(Long npid) {
        SQLiteConnection sqLiteConnection = new SQLiteConnection(this, "primaryDB");
        SQLiteDatabase db = sqLiteConnection.getReadableDatabase();

        Cursor cursor = db.rawQuery(QUERY + npid.toString(), null);
        cursor.moveToFirst();
        String partyDate = cursor.getString(2);

        cursor.close();
        db.close();
        return partyDate;
    }

    private void toolbarInit(String partyName, String partyDate) {
        Toolbar toolbar = findViewById(R.id.party_details_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(partyName + " - " + partyDate);
    }

    private void viewPagerInit(Long pid) {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.party_details_tabs);
        tabs.setupWithViewPager(viewPager);

        Bundle b = new Bundle();
        b.putLong("PartyID", pid);
        MemberFragment mf = new MemberFragment();
        mf.setArguments(b);
    }

    public void conclude(View view) {
        SQLiteDatabase db = sqLiteConnection.getWritableDatabase();
        db.execSQL("UPDATE PartyDB SET Concluded = 1 WHERE Id = " + pID.toString());
        db.close();
    }

    public void addMemberSupply(View view) {
        Intent intent = new Intent(this, NewMemberSupply.class);
        intent.putExtra("PartyID", pID);
        startActivity(intent);
    }
}
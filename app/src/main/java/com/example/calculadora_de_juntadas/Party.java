package com.example.calculadora_de_juntadas;

public class Party {
    private String name;
    private String date;
    private String description;
    private Boolean concluded;


    public Party(String name, String date, String description, Boolean concluded){
        this.name = name;
        this.date = date;
        this.description = description;
        this.concluded = concluded;
    }

    public Boolean getPartyConcluded() {
        return concluded;
    }

    public void setPartyConcluded(Boolean concluded) {
        this.concluded = concluded;
    }

    public String getPartyDate() {
        return date;
    }

    public void setPartyDate(String date) {
        this.date = date;
    }

    public String getPartyName() {
        return name;
    }

    public void setPartyName(String title) {
        this.name = title;
    }

    public String getPartyDescription() {
        return description;
    }

    public void setPartyDescription(String description) {
        this.description = description;
    }
}

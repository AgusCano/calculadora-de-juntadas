package com.example.calculadora_de_juntadas;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class NewParty extends AppCompatActivity {
    private EditText new_party_name_form, new_party_date_form, new_party_description_form,
            new_party_member_form;
    private ArrayList<Member> memberList;
    private RecyclerView memberRecycler;
    private MemberAdapter memberAdapter;
    private SQLiteConnection sqLiteConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_party);

        Toolbar toolbar = findViewById(R.id.new_party_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initForms();
        initDbConnection();
        memberRecyclerInit();
        memberAdapt();
    }

    public void initForms() {
        this.new_party_name_form = findViewById(R.id.new_party_name_form);
        this.new_party_date_form = findViewById(R.id.new_party_date_form);
        this.new_party_description_form = findViewById(R.id.new_party_description_form);
        this.new_party_member_form = findViewById(R.id.new_party_member_form);
    }

    public void initDbConnection() {
        sqLiteConnection = new SQLiteConnection(this, "primaryDB");
    }

    public void createNewMember(View view) {
        String nmName = this.new_party_member_form.getText().toString();
        if (nmName.matches("")) {
            Toast.makeText(this, "Ingresa el nombre del nuevo participante", Toast.LENGTH_SHORT).show();
            return;
        }

        for (int i=0; i<memberList.size(); i++) {
            if (memberList.get(i).getMemberName().matches(nmName)) {
                Toast.makeText(this, "El nombre ingresado está repetido para esta juntada", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        memberList.add(new Member(nmName, 0.0f));
        this.new_party_member_form.getText().clear();
    }

    public void deleteNewMember(Integer position) {
        memberList.remove(memberList.get(position));
        memberAdapter.notifyDataSetChanged();
    }

    public void memberRecyclerInit() {
        memberList = new ArrayList<>();
        memberRecycler = findViewById(R.id.new_party_recycler_view);
        LinearLayoutManager memberRecyclerLayout = new LinearLayoutManager(this);
        memberRecycler.setLayoutManager(memberRecyclerLayout);

        SpacingItemDecorator spacingItemDecorator = new SpacingItemDecorator(3);
        memberRecycler.addItemDecoration(spacingItemDecorator);
    }

    public void memberAdapt() {
        memberAdapter = new MemberAdapter(memberList);
        MemberAdapter.OnItemClickListener btnListener = (new MemberAdapter.OnItemClickListener() {
            @Override
            public void onDeleteMember(String mName, int position, Boolean bool) {
                deleteNewMember(position);
            }
        });
        memberAdapter.setOnItemClickListener(btnListener);
        memberRecycler.setAdapter(memberAdapter);
        memberAdapter.notifyDataSetChanged();
    }

    public void createNewParty(View view) {
        String npName = this.new_party_name_form.getText().toString();
        String npDate = this.new_party_date_form.getText().toString();
        String npDescription = this.new_party_description_form.getText().toString();

        if (npName.matches("")) {
            Toast.makeText(this, "Ingresa el nombre de la juntada primero", Toast.LENGTH_SHORT).show();
            return;
        }

        if (npDate.matches("")) {
            Toast.makeText(this, "Ingresa la fecha de la juntada primero", Toast.LENGTH_SHORT).show();
            return;
        }

        if (npDescription.matches("")) {
            Toast.makeText(this, "Ingresa una descripcion de la juntada primero", Toast.LENGTH_SHORT).show();
            return;
        }

        if (memberList.isEmpty()) {
            Toast.makeText(this, "Ingresa al menos un participante primero", Toast.LENGTH_SHORT).show();
            return;
        }

        SQLiteDatabase db = this.sqLiteConnection.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Name", npName);
        values.put("Date", npDate);
        values.put("Description", npDescription);
        values.put("Concluded", 0);
        Long npid = db.insert("PartyDB", "Id" , values);

        for (int i = 0; i < memberList.size(); i++) {
            values = new ContentValues();
            values.put("Name", memberList.get(i).getMemberName());
            values.put("Balance", 0.0);
            values.put("Party_id", npid);
            db.insert("MemberDB", "Id" , values);
        }

        db.close();

        Intent intent = new Intent(this, PartyDetails.class);
        intent.putExtra("PartyID", npid);
        //intent.putExtra("PartyListORNewParty", "NewParty");
        startActivity(intent);
    }
}

package com.example.calculadora_de_juntadas;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;


public class SupplyFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private Context c;

    //Supplys Recycler.
    private ArrayList<Supply> supplyList;
    private RecyclerView supplyRecycler;
    //Sqlite connection.
    private SQLiteConnection sqLiteConnection;
    //Supplys view
    private View view;

    public SupplyFragment() {
        // Required empty public constructor
    }

    public static SupplyFragment newInstance(String param1, String param2) {
        SupplyFragment fragment = new SupplyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.supply_fragment, container, false);

        this.c = getActivity();
        Long pid = ((PartyDetails)c).pID;

        //init supply recycler view
        supplyRecyclerInit();
        supplyLoad(pid);
        supplyAdapt();

        return view;
    }

    public void supplyRecyclerInit() {
        supplyList = new ArrayList<>();
        supplyRecycler = this.view.findViewById(R.id.supply_fragment_recycler_view);
        LinearLayoutManager supplyRecyclerLayout = new LinearLayoutManager(getActivity());
        supplyRecyclerLayout.setReverseLayout(true);
        supplyRecyclerLayout.setStackFromEnd(true);
        supplyRecycler.setLayoutManager(supplyRecyclerLayout);

        SpacingItemDecorator spacingItemDecorator = new SpacingItemDecorator(25);
        supplyRecycler.addItemDecoration(spacingItemDecorator);
    }

    private void supplyLoad(Long pid) {
        sqLiteConnection = new SQLiteConnection(getContext(), "primaryDB");
        SQLiteDatabase db = this.sqLiteConnection.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM SupplyDB WHERE party_id = " +
                                     pid.toString(), null);

        while(cursor.moveToNext()) {
            String name = cursor.getString(1);
            Float cost = cursor.getFloat(2);
            String description = cursor.getString(3);
            Supply supply = new Supply(name, cost, description);
            supplyList.add(supply);
        }

        cursor.close();
        db.close();
    }

    public void supplyAdapt() {
        final SupplyAdapter adapter = new SupplyAdapter(supplyList);
        supplyRecycler.setAdapter(adapter);

        adapter.setOnItemClickListener(new SupplyAdapter.OnItemClickListener() {
            public void onDeleteSupply(String sName, int position, Boolean bool) {
                Long pId = ((PartyDetails)c).pID;

                SQLiteDatabase db = sqLiteConnection.getReadableDatabase();
                db.execSQL("DELETE FROM SupplyDB WHERE Party_id = "
                        + pId + " AND Name = '" + sName +"'");

                db.close();

                int pos = 0;
                for (int i=0; i<supplyList.size(); i++) {
                    if(supplyList.get(i).getSupplyName().equals(sName)) {pos = i;}
                }

                supplyList.remove(pos);
                Toast.makeText(c, "Item eliminado", Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }
        });
    }
}

package com.example.calculadora_de_juntadas;

public class Supply {
    private String name;
    private Float cost;
    private String description;


    public Supply(String name, Float cost, String description){
        this.name = name;
        this.cost = cost;
        this.description = description;
    }

    public void setSupplyName(String name) { this.name = name; }

    public String getSupplyName() { return name; }

    public void setSupplyCost(Float cost) { this.cost = cost; }

    public Float getSupplyCost() { return cost; }

    public void setSupplyDescription(String description) { this.description = description; }

    public String getSupplyDescription() { return description; }
}

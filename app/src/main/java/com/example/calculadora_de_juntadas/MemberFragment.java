package com.example.calculadora_de_juntadas;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


public class MemberFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private Context c;

    //Members Recycler.
    private ArrayList<Member> memberList;
    private RecyclerView memberRecycler;
    //Sqlite connection.
    private SQLiteConnection sqLiteConnection;
    //Members View
    private View view;

    public MemberFragment() {
        // Required empty public constructor
    }

    public static MemberFragment newInstance(String param1, String param2) {
        MemberFragment fragment = new MemberFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view =  inflater.inflate(R.layout.member_fragment, container, false);

        this.c = getActivity();
        Long pid = ((PartyDetails)c).pID;

        //init member recycler view
        memberRecyclerInit();

        memberAdapt();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Long pid = ((PartyDetails)c).pID;
        memberLoad(pid);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void memberRecyclerInit() {
        memberList = new ArrayList<>();
        memberRecycler = this.view.findViewById(R.id.member_fragment_recycler_view);
        LinearLayoutManager memberRecyclerLayout = new LinearLayoutManager(getActivity());
        memberRecyclerLayout.setReverseLayout(true);
        memberRecyclerLayout.setStackFromEnd(true);
        memberRecycler.setLayoutManager(memberRecyclerLayout);

        SpacingItemDecorator spacingItemDecorator = new SpacingItemDecorator(25);
        memberRecycler.addItemDecoration(spacingItemDecorator);
    }

    private void memberLoad(Long pid) {
        sqLiteConnection = new SQLiteConnection(getContext(), "primaryDB");
        SQLiteDatabase db = this.sqLiteConnection.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM MemberDB WHERE party_id = " +
                                    pid.toString(), null);

        while(cursor.moveToNext()) {
            String name = cursor.getString(1);
            Float balance = cursor.getFloat(2);
            Member member = new Member(name, balance);
            memberList.add(member);
        }

        cursor.close();
        db.close();
    }

    public void memberAdapt() {
        final MemberAdapter adapter = new MemberAdapter(memberList);
        memberRecycler.setAdapter(adapter);

        adapter.setOnItemClickListener(new MemberAdapter.OnItemClickListener() {
            public void onDeleteMember(String mName, int position, Boolean bool) {
                Long pId = ((PartyDetails)c).pID;

                SQLiteDatabase db = sqLiteConnection.getReadableDatabase();
                db.execSQL("DELETE FROM MemberDB WHERE Party_id = "
                           + pId + " AND Name = '" + mName +"'");

                db.close();

                int pos = 0;
                for (int i=0; i<memberList.size(); i++) {
                    if(memberList.get(i).getMemberName().equals(mName)) {pos = i;}
                }

                memberList.remove(pos);
                Toast.makeText(c, "Participante eliminado", Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }
        });
    }
}

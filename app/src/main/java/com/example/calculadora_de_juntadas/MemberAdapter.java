package com.example.calculadora_de_juntadas;
import android.annotation.SuppressLint;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class MemberAdapter
        extends RecyclerView.Adapter<MemberAdapter.ViewHolderMember>
        implements View.OnClickListener {
    private ArrayList<Member> memberList;
    private OnItemClickListener memberListener;


    public MemberAdapter(ArrayList<Member> MemberList) {
        this.memberList = MemberList;
    }

    @Override
    public void onClick(View v) {

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        memberListener = listener;
    }

    public interface OnItemClickListener {
        void onDeleteMember(String memberName, int position, Boolean bool);
    }

    public class ViewHolderMember extends RecyclerView.ViewHolder {
        TextView memberName;
        TextView memberBalance;
        ImageButton deleteMember;

        public ViewHolderMember(View itemView, final OnItemClickListener listener) {
            super(itemView);
            memberName = itemView.findViewById(R.id.member_name);
            memberBalance = itemView.findViewById(R.id.member_balance);
            deleteMember = itemView.findViewById(R.id.member_remove_member);

            deleteMember.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        String mName = memberName.getText().toString();
                        listener.onDeleteMember(mName, 0, Boolean.TRUE);
                    }
                }
            });
        }
    }

    @Override
    public MemberAdapter.ViewHolderMember onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.member, parent, false);
        view.setOnClickListener(this);

        return new ViewHolderMember(view, memberListener);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(MemberAdapter.ViewHolderMember holder, int position) {
        holder.memberName.setText(memberList.get(position).getMemberName());

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.memberBalance.setText(df.format(memberList.get(position).getMemberBalance()));
    }

    @Override
    public int getItemCount() {
        return memberList.size();
    }
}

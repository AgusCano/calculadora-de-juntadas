package com.example.calculadora_de_juntadas;


public class Member {
    private String name;
    private Float balance;


    public Member(String name, Float balance){
        this.name = name;
        this.balance = balance;
    }

    public void setMemberName(String name) { this.name = name; }

    public String getMemberName() { return name; }

    public void setMemberBalance(Float balance) { this.balance = balance; }

    public Float getMemberBalance() { return balance; }
}

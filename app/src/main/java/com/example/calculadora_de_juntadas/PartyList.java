package com.example.calculadora_de_juntadas;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import java.util.ArrayList;


public class PartyList extends AppCompatActivity implements PartyAdapter.PartyClickListener {
    private ArrayList<Party> partyList;
    private RecyclerView partyRecycler;
    private SQLiteConnection sqLiteConnection;
    private final static String QUERY = "SELECT * FROM PartyDB WHERE Name = ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.party_list);
        Toolbar toolbar = findViewById(R.id.party_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sqLiteConnection = new SQLiteConnection(this, "primaryDB");
        partyRecyclerInit();
        partyLoad();
        partyAdapt();
    }

    public void goToNewParty(View view) {
        Intent intent = new Intent(this, NewParty.class);
        startActivity(intent);
    }

    private void partyRecyclerInit() {
        partyList = new ArrayList<>();
        partyRecycler = findViewById(R.id.party_recycler_view);
        LinearLayoutManager partyRecyclerLayout = new LinearLayoutManager(this);
        partyRecyclerLayout.setReverseLayout(true);
        partyRecyclerLayout.setStackFromEnd(true);
        partyRecycler.setLayoutManager(partyRecyclerLayout);

        SpacingItemDecorator spacingItemDecorator = new SpacingItemDecorator(25);
        partyRecycler.addItemDecoration(spacingItemDecorator);
    }

    private void partyLoad() {
        SQLiteDatabase db = sqLiteConnection.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM PartyDB", null);
        while(cursor.moveToNext()) {
            String name = cursor.getString(1);
            String date = cursor.getString(2);
            String description = cursor.getString(3);
            Boolean concluded;
            if(cursor.getInt(4) == 1) {
                concluded = Boolean.TRUE;
            } else {
                concluded = Boolean.FALSE;
            }
            Party party = new Party(name, date, description, concluded);
            partyList.add(party);
        }
        cursor.close();
        db.close();
    }

    /*private void partyLoad() {
        this.partyList.add(new Party("Prueba0", "2020-03-06", "descripcion", Boolean.TRUE));
        this.partyList.add(new Party("Prueba1", "2020-03-09", "descripcion", Boolean.FALSE));
        this.partyList.add(new Party("Prueba2", "2020-03-11", "descripcion", Boolean.FALSE));
        this.partyList.add(new Party("Prueba3", "2020-03-11", "descripcion", Boolean.TRUE));
        this.partyList.add(new Party("Prueba4", "2020-03-11", "descripcion", Boolean.TRUE));
    }*/

    public void partyAdapt() {
        PartyAdapter adapter = new PartyAdapter(this.partyList, this);
        partyRecycler.setAdapter(adapter);
    }

    @Override
    public void onPartyClick(int pos) {
        String partyName = partyList.get(pos).getPartyName();
        Long partyID = getPartyID(partyName);

        Intent intent = new Intent(this, PartyDetails.class);
        intent.putExtra("PartyID", partyID);
        //intent.putExtra("PartyListORNewParty", "PartyList");
        startActivity(intent);
    }

    private Long getPartyID(String partyName) {
        SQLiteConnection sqLiteConnection = new SQLiteConnection(this, "primaryDB");
        SQLiteDatabase db = sqLiteConnection.getReadableDatabase();

        Cursor cursor = db.rawQuery(QUERY + String.format( "\"%s\"", partyName), null);
        cursor.moveToFirst();
        Long partyID = cursor.getLong(0);

        cursor.close();
        db.close();
        return partyID;
    }
}

package com.example.calculadora_de_juntadas;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

public class MainScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
    }

    public void goToNewParty(View view) {
        Intent intent = new Intent(this, NewParty.class);
        startActivity(intent);
    }

    public void goToPartyList(View view) {
        Intent intent = new Intent(this, PartyList.class);
        startActivity(intent);
    }
}

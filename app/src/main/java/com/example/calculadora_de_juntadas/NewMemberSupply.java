package com.example.calculadora_de_juntadas;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static java.lang.Boolean.*;

public class NewMemberSupply extends AppCompatActivity {
    private EditText new_member_form, new_supply_name_form, new_supply_total, new_supply_description,
            new_purchase_name, new_purchase_amount;
    private SQLiteConnection sqLiteConnection;
    private ArrayList<Member> memberList;
    private RecyclerView memberRecycler;
    private MemberAdapter memberAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_member_supply);
        Toolbar toolbar = findViewById(R.id.new_member_supply_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initForms();
        initDbConnection();
        memberRecyclerInit();
        memberAdapt();
    }

    public void initForms() {
        this.new_member_form = findViewById(R.id.new_member_supply_form0);
        this.new_supply_name_form = findViewById(R.id.new_member_supply_form1);
        this.new_supply_total = findViewById(R.id.new_member_supply_form2);
        this.new_supply_description = findViewById(R.id.new_member_supply_form3);
        this.new_purchase_name = findViewById(R.id.new_member_supply_form4);
        this.new_purchase_amount = findViewById(R.id.new_member_supply_form5);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Long partyID = getIntent().getLongExtra("PartyID", 1);

                Intent intent = new Intent(this, PartyDetails.class);
                intent.putExtra("PartyID", partyID);
                startActivity(intent);
                break;
        }
        return true;
    }

    public void initDbConnection() {
        sqLiteConnection = new SQLiteConnection(this, "primaryDB");
    }

    public void deleteNewMember(Integer position) {
        memberList.remove(memberList.get(position));
        memberAdapter.notifyDataSetChanged();
    }

    public void memberRecyclerInit() {
        memberList = new ArrayList<>();
        memberRecycler = findViewById(R.id.new_member_supply_recycler_view);
        LinearLayoutManager memberRecyclerLayout = new LinearLayoutManager(this);
        memberRecycler.setLayoutManager(memberRecyclerLayout);

        SpacingItemDecorator spacingItemDecorator = new SpacingItemDecorator(3);
        memberRecycler.addItemDecoration(spacingItemDecorator);
    }

    public void memberAdapt() {
        memberAdapter = new MemberAdapter(memberList);
        MemberAdapter.OnItemClickListener btnListener = (new MemberAdapter.OnItemClickListener() {
            @Override
            public void onDeleteMember(String mName, int position, Boolean bool) {
                deleteNewMember(position);
            }
        });
        memberAdapter.setOnItemClickListener(btnListener);
        memberRecycler.setAdapter(memberAdapter);
        memberAdapter.notifyDataSetChanged();
    }

    public void createNewMember(View view) {
        String nmName = this.new_member_form.getText().toString();

        if (nmName.matches("")) {
            Toast.makeText(this, "Ingresa el nombre del nuevo participante", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean exists = FALSE;
        Long pid = getIntent().getLongExtra("PartyID", 1);
        SQLiteDatabase db = this.sqLiteConnection.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM MemberDB WHERE party_id = " +
                pid.toString(), null);

        while(cursor.moveToNext()) {
            if (cursor.getString(1).equals(nmName)) exists = TRUE;
        }

        cursor.close();
        db.close();

        if (exists == TRUE) {
            Toast.makeText(this, "Ya existe un participante con este nombre", Toast.LENGTH_SHORT).show();
            return;
        }

        SQLiteDatabase dbw = this.sqLiteConnection.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Name", nmName);
        values.put("Balance", 0);
        values.put("Party_id", pid);
        dbw.insert("MemberDB", "Id" , values);
        dbw.close();
        Toast.makeText(this, "Participante agregado", Toast.LENGTH_SHORT).show();

        this.new_member_form.getText().clear();
    }

    public void createNewPurchase(View view) {
        String nmName = this.new_purchase_name.getText().toString();
        String nmAmount = this.new_purchase_amount.getText().toString();

        if (nmName.matches("")) {
            Toast.makeText(this, "Ingresa el nombre del participante", Toast.LENGTH_SHORT).show();
            return;
        }
        if (nmAmount.matches("")) {
            Toast.makeText(this, "Ingresa el monto aportado por el participante", Toast.LENGTH_SHORT).show();
            return;
        }

        for (int i=0; i<memberList.size(); i++) {
            if (memberList.get(i).getMemberName().matches(nmName)) {
                Toast.makeText(this, "El nombre ingresado está repetido para este item", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        boolean exists = FALSE;
        Long pid = getIntent().getLongExtra("PartyID", 1);
        SQLiteDatabase db = this.sqLiteConnection.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM MemberDB WHERE party_id = " +
                pid.toString(), null);

        while(cursor.moveToNext()) {
            if (cursor.getString(1).equals(nmName)) exists = TRUE;
        }

        cursor.close();
        db.close();

        if (exists == FALSE) {
            Toast.makeText(this, "No existe un participante con este nombre", Toast.LENGTH_SHORT).show();
            return;
        }

        memberList.add(new Member(nmName, Float.valueOf(nmAmount)));
        this.new_purchase_name.getText().clear();
        this.new_purchase_amount.getText().clear();
    }

    public void createNewSupply(View view) {
        String nsName = this.new_supply_name_form.getText().toString();
        String nsAmount = this.new_supply_total.getText().toString();
        String nsDescription = this.new_supply_description.getText().toString();

        if (nsName.matches("")) {
            Toast.makeText(this, "Ingresa el nombre del nuevo item", Toast.LENGTH_SHORT).show();
            return;
        }
        if (nsAmount.matches("")) {
            Toast.makeText(this, "Ingresa el costo total del nuevo item", Toast.LENGTH_SHORT).show();
            return;
        }

        float sum = 0;
        for (int i=0; i<memberList.size(); i++) {
            sum += memberList.get(i).getMemberBalance();
        }
        if (Float.parseFloat(nsAmount) != sum) {
            Toast.makeText(this, "El costo total y la suma de los montos no coincide", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean exists;
        ArrayList<Integer> membersId = new ArrayList<>();
        ArrayList<Float> membersBalance = new ArrayList<>();
        Long pid = getIntent().getLongExtra("PartyID", 1);
        this.sqLiteConnection = new SQLiteConnection(this, "primaryDB");
        SQLiteDatabase db = this.sqLiteConnection.getReadableDatabase();

        for (int i=0; i<memberList.size(); i++) {
            exists = FALSE;
            String mName = memberList.get(i).getMemberName();
            Cursor cursor = db.rawQuery("SELECT * FROM MemberDB WHERE party_id = " +
                    pid.toString(), null);

            while(cursor.moveToNext()) {
                if (cursor.getString(1).equals(mName)) {
                    exists = TRUE;
                    membersId.add(Integer.valueOf(cursor.getString(0)));
                    membersBalance.add(0 - memberList.get(i).getMemberBalance());
                }
            }

            cursor.close();

            if (exists != TRUE) {
                Toast.makeText(this, "No existe el participante " + mName + " en esta juntada",
                               Toast.LENGTH_SHORT).show();
                return;
            }
        }
        db.close();

        SQLiteDatabase dbs = this.sqLiteConnection.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Name", nsName);
        values.put("Cost", nsAmount);
        values.put("Description", nsDescription);
        values.put("Party_id", pid);
        Long nsId = dbs.insert("SupplyDB", "Id" , values);

        dbs.close();

        SQLiteDatabase dbp = this.sqLiteConnection.getWritableDatabase();
        for (int i=0; i<memberList.size(); i++) {
            values = new ContentValues();
            values.put("Member_id", membersId.get(i));
            values.put("Supply_id", nsId);
            values.put("Amount", membersBalance.get(i));
            dbp.insert("PurchaseDB", "Id" , values);
        }

        dbp.close();

        Log.d("mester", String.valueOf(membersId));
        Log.d("mester", String.valueOf(membersBalance));

        SQLiteDatabase dbm = this.sqLiteConnection.getWritableDatabase();
        for (int i=0; i<membersId.size(); i++) {
            Cursor cursor = dbm.rawQuery("SELECT Balance FROM MemberDB WHERE party_id = " +
                                        pid.toString() + " AND Id = " + membersId.get(i).toString(),
                                        null);
            cursor.moveToFirst();
            Float bal = membersBalance.get(i) + Float.parseFloat(cursor.getString(0));
            dbm.execSQL("UPDATE MemberDB SET Balance = " + bal.toString() + " WHERE Id = " +
                        membersId.get(i).toString());
            cursor.close();
        }
        dbm.close();

        Toast.makeText(this, "Item agregado", Toast.LENGTH_SHORT).show();
        this.new_supply_name_form.getText().clear();
        this.new_supply_description.getText().clear();
        this.new_supply_total.getText().clear();
        this.memberList.clear();
        this.memberAdapter.notifyDataSetChanged();
    }
}

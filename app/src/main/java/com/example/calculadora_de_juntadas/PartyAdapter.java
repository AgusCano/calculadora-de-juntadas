package com.example.calculadora_de_juntadas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;


public class PartyAdapter extends RecyclerView.Adapter<PartyAdapter.ViewHolderParty>{
    private ArrayList<Party> partyList;
    private PartyClickListener partyClickListener;

    public PartyAdapter(ArrayList<Party> PartyList, PartyClickListener partyClickListener) {
        this.partyList = PartyList;
        this.partyClickListener = partyClickListener;
    }

    public interface PartyClickListener {
        void onPartyClick(int pos);
    }

    public class ViewHolderParty extends RecyclerView.ViewHolder implements View.OnClickListener {
        PartyClickListener partyListener;
        TextView title, date, description;
        ImageView concluded;

        public ViewHolderParty(View itemView, PartyClickListener partyListener) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.partyListener = partyListener;
            title = itemView.findViewById(R.id.party_title);
            date = itemView.findViewById(R.id.party_date);
            description = itemView.findViewById(R.id.party_description);
            concluded = itemView.findViewById(R.id.party_conclution_image);
        }

        @Override
        public void onClick(View v) {
            partyListener.onPartyClick(getAdapterPosition());
        }
    }

    @Override
    public PartyAdapter.ViewHolderParty onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.party,null, false);
        return new ViewHolderParty(view, partyClickListener);
    }

    @Override
    public void onBindViewHolder(PartyAdapter.ViewHolderParty holder, int position) {
        holder.title.setText(partyList.get(position).getPartyName());
        holder.date.setText(partyList.get(position).getPartyDate());
        holder.description.setText(partyList.get(position).getPartyDescription());
        if(partyList.get(position).getPartyConcluded()) {
            holder.concluded.setImageResource(R.color.colorGreen);
        } else {
            holder.concluded.setImageResource(R.color.colorRed);
        }
    }

    @Override
    public int getItemCount() {
        return partyList.size();
    }
}
